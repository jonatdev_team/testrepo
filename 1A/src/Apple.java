import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Stream;




/*
class Apple {
    static int func( int i){
        return  ++i;
    }     public static void main(String[] args) {
        int i=0;
        int j=func(i++) + i + func(++i);
        System.out.println(i);
        System.out.println(j);
    }
}


class Apple {
    public static void main(String [] args) {
            int x=20;
            String sup = (x < 15) ? "small" : (x < 22)? "tiny" : "huge";
            System.out.println(sup);
    }
}*/


public class Apple {      
	public static void leftshift(int i, int j) {
     i <<= j; 
    }      
	
	public static void main(String args[]) {
        int i = 4, j = 2; 
        leftshift(i, j); 
        System.out.println(i); 
    } 
}











public class Apple implements green,red{

	
	public static void main (String args[]) {
		 int x = 8;
	        System.out.println(++x * 3 + "," + x);
		
		System.out.print("testing\n");
		green.printColour();
		
		green a1 = (green) new Apple();
		System.out.print(a1.grr());
		red a2 = (red) new Apple();
		System.out.print(a2.red());
		
		//System.out.print(a.grr());
		//System.out.print(a.red());
		
		
		

		LinkedList<String> names = new LinkedList<String>();
		
		
		names.add("john");
		names.add("mark");
		names.add("wendy");
		names.add("adam");
		
		for (String name : names)
			System.out.println(name);
		
		Iterator i = names.iterator();
		while (i.hasNext())
		  {
		    String name = (String) i.next();
		    System.out.println(name);
		  }
		
		
		names.forEach(System.out::println);
		Stream.iterate(1, s -> s+1).limit(10).forEach(System.out::println);
		
		System.out.print("\n-------------------------------------------\n");
		
		//java 7
		 Collections.sort(names, new Comparator<String>() {
			    @Override
			      public int compare(String s1, String s2) {
			          return s1.length() - s2.length();
			      }
			  });
		//Java 8
		 Collections.sort(names, (s1, s2) -> s1.length() - s2.length());
		 

		// or
		 names.sort(Comparator.comparingInt(String::length));
		
		 names.forEach(System.out::println);
		 
		 
		 
		 names.sort(Comparator.naturalOrder());
		 
		 System.out.print("\n-------------------------------------------\n");
		 
		 List<String> l = Arrays.asList("","");
		 List<Object> l2 = Arrays.asList(names.toArray());
		 l2.forEach(System.out::print);
		 
		 System.out.print("\n-------------------------------------------\n");
		 
		  class Person{
			 
			  String fname="---";
			  String sname="---";
			 
			  String longname="---";
			  public Person(String s) {
				// TODO Auto-generated constructor stub
				  longname=s;
			   }
			void Person(String string) {
				// TODO Auto-generated constructor stub
			}
			String getFirstName() {return fname;};
			  String getLastName() {return sname;};
			 
			 
			 
		 }
		 
		 Person pa=new Person("");
		 pa.fname="adam";
		 pa.sname="zolt";
		 
		 Person pb=new Person("");
		 pb.fname="brian";
		 pb.sname="west";
		 
		 Person pc=new Person("");
		 pc.fname="cameran";
		 pc.sname="smith";
		 
		 Person pd=new Person("");
		 pd.fname="elias";
		 pd.sname="bord";
		 
		 
		 LinkedList<Person> p = new LinkedList<Person>();
		 p.add(pa);
		 p.add(pb);
		 p.add(pc);
		 p.add(pd);
		 
		 
		 
		 p.sort(Comparator.comparing(Person::getLastName).thenComparing(Person::getFirstName));
		 Collections.sort(p, new Comparator<Person>() {
			      @Override
			      public int compare(Person p1, Person p2) {
			          int n = p1.getLastName().compareTo(p2.getLastName());
			          if (n == 0) {
			              return p1.getFirstName().compareTo(p2.getFirstName());
			          }
			          return n;
			      }
			});
		 
		
		 
		 
		 
		//p.forEach(System.out::println );
		
		p.forEach(a -> { 
			      System.out.println(a.getFirstName()+"-"+a.getLastName());
			  });
		p.forEach(a ->  System.out.println(a.getFirstName()+"-"+a.getLastName()));

		
		Stream<Person> stream = p.stream();
		
		p.stream().forEach(a -> {
			System.out.println( a.getFirstName()+"-"+a.getLastName()); 
		});
		
		
		for (Object obj : p) {
			System.out.println( ((Person)obj).getFirstName() +"-" + "-"+ ((Person)obj).getLastName());

			}
		
		
		
		
		//------------------------------------------------------------------------------------//
		
		 Person longest = p.stream()
			        .max(Comparator.comparing(person -> ((Person)person).getFirstName().length()))
			        .get();
		System.out.println(longest.getFirstName());
		
		
		
		
		
		
		
		
		String reduced2 = names.stream()
		        .reduce((acc, item) -> acc  + item)
		        .get();
		System.out.println(">>"+reduced2);
		
		
		String reduced3 = names.stream()
				 .filter(item -> item.startsWith("a"))
		        .reduce((acc, item) -> acc  + item)
		       
		        .get();
		System.out.println(">>"+reduced3);
		
		
		//String reduced4 = 
		 //Collections.sort(names,  s1.compareToIgnoreCase(s2));
		 //names.forEach(System.out::println);
		
		
		  ArrayList<String> strings = names.stream().collect(
				  () -> new ArrayList<>(),(c, e) -> c.add(e.toString()),
                  (c1, c2) -> c1.addAll(c2));
		
		  System.out.println("-----"+strings);
		
		/*
		List<String> unavailable = list1.stream()
				  .filter(e -> (list2.stream()
				    .filter(d -> d.getStr().equals(e))
				    .count())<1)
				    .collect(Collectors.toList());
		
		*/
		
		
		
	}
	
	
	public String grr(){
		return "G";	
	}
	
	
	public String red(){
		return "R";	
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
